import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  formulaireArray:any[ ]=[ ];
  formulaire={id:0,Nom:'',Classe:'',note:''};
  newformulaire=false;
  addComment() {
   
      this.formulaire.id=this.formulaireArray.length+1;
        this.formulaireArray.push({
          id:this.formulaire.id,
          Nom:this.formulaire.Nom ,
          Classe:this.formulaire.Classe,
          note:this.formulaire.note

          });
          this.formulaire.Nom='';
          this.formulaire.Classe='';
          this.formulaire.note='';  
        
      }
      constructor() { }

  ngOnInit(): void {
  }
}
